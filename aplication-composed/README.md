## Aplicação para atribuição de limite de crédito ##

Esta é uma API simples que vai analisar o perfil de uma pessoa e através do seu SCORE, saber se tem atribuição de algum limite de crédito.

Este projeto é composto por dois Docker containers ligados pelo Compose e foi desenvolvido usando as seguintes tecnologias:

- Banco de Dados SQLServer
- Java Spring Boot
- Maven
- myBatis
- Swagger

Para acessar ao endpoint (principal), deverá ser usado o seguinte URL: **http://localhost:9002/**


### Banco de Dados SQLServer ###

Optei por usar um banco de dados remoto ao invés de seguir o caminho mais fácil de usar uma H2 ou HSQL embebido para mostrar outras funcionalidades

Este banco está hospedado remotamente num servidor publico

- Host: *sql5102.site4now.net*
- Port: *1433*
- Database: *DB_A70863_creditanalyser*
- User: *DB_A70863_creditanalyser_admin*
- Password: *P3ssw0rd*


### credit-score (REST API) ###

Este é um aplicativo baseado em Spring Boot (Java) que se conecta a um banco de dados que e expõe os endpoints REST que podem
ser consumido como um microserviço por outro endpoint. 
Neste caso, ele apenas expôs métodos HTTP REST como o GET.

A lista completa de endpoints REST disponíveis pode ser encontrada na IU Swagger, que pode ser chamada usando o link: **http://localhost:9001/**

Este aplicativo também é colocado no contêiner do Docker e sua definição pode ser encontrada em um arquivo * credit-score / Dockerfile *. 

### credit-analysis (REST API) ###

Este é um aplicativo é bastante semelhante ao anterior, só que ele consome o endpoint **credit-score** para retornar o resultado e o score.
É importante referir aqui que este endpoint para poder comunicar com o outro teve de se criar um network e usar o container-name como hostname para que internamente os containers possam comunicar dentro do compose. 

A lista completa de endpoints REST disponíveis pode ser encontrada na IU Swagger, que pode ser chamada usando o link: **http://localhost:9002/**

Este endpoint seria o usado por uma aplicação Frontend que fosse desenvolvida.


#### Quais são os pre-requisitos? ####

- instalar no client: docker e docker-compose
- instalar no client: git
- maven (não precisa porque a build está sendo executado dentro de cada um dos containers)

#### Como faço para rodar? ####

Para rodar este projeto:

1. Fazer CLONE do repositório BitButcket
	`git clone https://tuxoPT@bitbucket.org/tuxoPT/myrepo.git`
2. Entrar na pasta clonada
	`cd .\myrepo\aplication-composed\`
3. Abrir o Docker-CLI ou Terminal e executar:
	`docker-compose up`
	
#### Docker Composed Imagens ####

![picture](https://fuelag-my.sharepoint.com/personal/mauro_sousa_fuel_ag/Documents/projetos-java/upload-imagens/Images-List.png)


#### Imagem do Docker Containers Compostos ####

![picture](https://fuelag-my.sharepoint.com/personal/mauro_sousa_fuel_ag/Documents/projetos-java/upload-imagens/Containers-List.png)

#### Projeto sincronizado com o DockerHUB ####

**repositório privado**

![picture](https://fuelag-my.sharepoint.com/personal/mauro_sousa_fuel_ag/Documents/projetos-java/upload-imagens/dockerhub.png)
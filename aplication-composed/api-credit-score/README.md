### API de Regras de SCORE

Microserviço Spring Boot que calcula o Score com base nas regras


Acessar ao URL [**http://localhost:9001/**](http://localhost:9001/)

#### EndPoints Expostos ####

![picture](https://fuelag-my.sharepoint.com/personal/mauro_sousa_fuel_ag/Documents/projetos-java/upload-imagens/credit-score-api.png)


#### Exemplo de um resultado ####

![picture](https://fuelag-my.sharepoint.com/personal/mauro_sousa_fuel_ag/Documents/projetos-java/upload-imagens/credit-score-api-result.png)
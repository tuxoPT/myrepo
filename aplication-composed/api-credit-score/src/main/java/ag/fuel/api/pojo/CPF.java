package ag.fuel.api.pojo;

import java.io.Serializable;

public class CPF implements Serializable, Cloneable {

    private String CPF;

    public CPF() {

    }

    public ag.fuel.api.pojo.CPF clone() {

        ag.fuel.api.pojo.CPF clone = null;

        try {
            clone = (ag.fuel.api.pojo.CPF) super.clone();
        } catch (CloneNotSupportedException e) {

            throw new RuntimeException(e);
        }

        return clone;
    }

    public CPF(String CPF) {
        this.CPF = CPF;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }
}

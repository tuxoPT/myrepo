/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.controllers;

import ag.fuel.api.dao.ScoreRulesDAO;
import ag.fuel.api.dao.impl.ScoreRulesDAOImpl;
import ag.fuel.api.pojo.CPF;
import ag.fuel.api.pojo.ProfileScoreResult;
import ag.fuel.api.utils.CPFCNPJUtils;
import ag.fuel.api.utils.GlobalProperties;
import ag.fuel.api.utils.LogSLF4J;
import ag.fuel.api.utils.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mauro Sousa
 */
@Api(tags = "Servico Atribuicao Credito", description = "Regras para Cálculo de Score")
@RestController
public class CreditScoreWebservicesController {

    @ApiOperation(value = "Obter Score de um perfil", notes = "Serviço para ser executado num microserviço", response = RestResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK - Get values"),
            @ApiResponse(code = 204, message = "NO_CONTENT - Doesn't get values"),
            @ApiResponse(code = 500, message = "INTERNAL SERVER ERROR - An Exception Occurred")})
    @RequestMapping(path = "/get-perfil-credito/{nome}/{idade}/{cpf}/{dependentes}/{renda}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<RestResponse> getProfileScore(@PathVariable(value = "nome") String nome, @PathVariable(value = "idade") int idade, @PathVariable(value = "cpf") String cpf, @PathVariable(value = "dependentes") int dependentes, @PathVariable(value = "renda") int renda) {

        ProfileScoreResult retProfileScoreResult = new ProfileScoreResult();
        ScoreRulesDAO scoreRulesDAO = new ScoreRulesDAOImpl();
        CPFCNPJUtils cpfcnpjUtils = new CPFCNPJUtils();

        RestResponse restResponse;

        try {
            if (nome.isBlank() || cpf.isBlank() || idade == 0 || renda == 0) {
                ProfileScoreResult newProfileScoreResult = new ProfileScoreResult();
                newProfileScoreResult.setNome(nome);
                newProfileScoreResult.setCPF(cpf);
                restResponse = new RestResponse(newProfileScoreResult, HttpStatus.OK, HttpStatus.OK.value(), "Dados inválidos para análise!");
                return new ResponseEntity<>(restResponse, HttpStatus.OK);
            } else {
                boolean verificaCPF = cpfcnpjUtils.isCPF(cpf);
                if (verificaCPF) {
                    retProfileScoreResult = scoreRulesDAO.getProfileScoreResult(nome, idade, cpfcnpjUtils.imprimeCPF(cpf), dependentes, renda);
                    scoreRulesDAO.getSession().close();
                    if (retProfileScoreResult == null) {
                        ProfileScoreResult newProfileScoreResult = new ProfileScoreResult();
                        newProfileScoreResult.setNome(nome);
                        newProfileScoreResult.setCPF(cpf);
                        restResponse = new RestResponse(newProfileScoreResult, HttpStatus.OK, HttpStatus.OK.value(), "Regra Inexistente para esse Score!");
                        return new ResponseEntity<>(restResponse, HttpStatus.OK);
                    } else {
                        restResponse = new RestResponse(retProfileScoreResult, HttpStatus.OK, HttpStatus.OK.value(), GlobalProperties.MESSAGE_SUCCESS);
                        return new ResponseEntity<>(restResponse, HttpStatus.OK);
                    }
                } else {
                    retProfileScoreResult.setNome(nome);
                    retProfileScoreResult.setCPF(cpf);
                    restResponse = new RestResponse(retProfileScoreResult, HttpStatus.OK, HttpStatus.OK.value(), "CPF Inválido!");
                    return new ResponseEntity<>(restResponse, HttpStatus.OK);
                }
            }
        } catch (Exception ex) {
            LogSLF4J.logException(this.getClass(), ex);
            restResponse = new RestResponse(LogSLF4J.getException(this.getClass(), ex), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), GlobalProperties.MESSAGE_ERROR);

            return new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Obter todas as regras de atribuição de Score", notes = "Serviço para teste de obter regras", response = RestResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK - Get values"),
            @ApiResponse(code = 204, message = "NO_CONTENT - Doesn't get values"),
            @ApiResponse(code = 500, message = "INTERNAL SERVER ERROR - An Exception Occurred")})
    @RequestMapping(path = "/get-todas-regras-score", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<RestResponse> getAllScoreRules() {

        List<Object> objectList = new ArrayList<>();
        ScoreRulesDAO scoreRulesDAO = new ScoreRulesDAOImpl();

        RestResponse restResponse;

        try {
            objectList = scoreRulesDAO.getAllScoreRules();
            scoreRulesDAO.getSession().close();

            restResponse = new RestResponse(objectList, HttpStatus.OK, HttpStatus.OK.value(), GlobalProperties.MESSAGE_SUCCESS);
            return new ResponseEntity<>(restResponse, HttpStatus.OK);
        } catch (Exception ex) {
            LogSLF4J.logException(this.getClass(), ex);
            restResponse = new RestResponse(LogSLF4J.getException(this.getClass(), ex), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), GlobalProperties.MESSAGE_ERROR);

            return new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Gerar um CPF Válido", notes = "Serviço de teste para validar CPF", response = RestResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK - Get values"),
            @ApiResponse(code = 204, message = "NO_CONTENT - Doesn't get values"),
            @ApiResponse(code = 500, message = "INTERNAL SERVER ERROR - An Exception Occurred")})
    @RequestMapping(path = "/gerar-cpf/{quantidade}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<RestResponse> gerarCPF(@PathVariable(value = "quantidade") int quantidade) {

        List<Object> objectList = new ArrayList<>();

        RestResponse restResponse;

        try {
            for (int i = 0; i < quantidade; i++) {
                CPFCNPJUtils gerador = new CPFCNPJUtils();
                CPF cpf = new CPF();
                cpf.setCPF(gerador.cpf(true));
                objectList.add(cpf);
            }

            restResponse = new RestResponse(objectList, HttpStatus.OK, HttpStatus.OK.value(), GlobalProperties.MESSAGE_SUCCESS);
            return new ResponseEntity<>(restResponse, HttpStatus.OK);
        } catch (Exception ex) {
            LogSLF4J.logException(this.getClass(), ex);
            restResponse = new RestResponse(LogSLF4J.getException(this.getClass(), ex), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), GlobalProperties.MESSAGE_ERROR);

            return new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.dao;

import ag.fuel.api.pojo.ProfileScoreResult;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author Mauro Sousa
 */
public interface ScoreRulesDAO {

    SqlSession getSession();

    void setSession(SqlSession session);

    ProfileScoreResult getProfileScoreResult(@PathVariable(value = "nome") String nome, @PathVariable(value = "idade") int idade, @PathVariable(value = "cpf") String cpf, @PathVariable(value = "dependentes") int dependentes, @PathVariable(value = "renda") int renda) throws Exception;

    List<Object> getAllScoreRules() throws Exception;
}

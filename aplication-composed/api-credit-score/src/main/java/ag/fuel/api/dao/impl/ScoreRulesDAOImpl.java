/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.dao.impl;

import ag.fuel.api.dao.ScoreRulesDAO;
import ag.fuel.api.mybatis.MyBatisUtil;
import ag.fuel.api.pojo.ProfileScoreResult;
import ag.fuel.api.utils.LogSLF4J;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Mauro Sousa
 */
public class ScoreRulesDAOImpl implements ScoreRulesDAO {

    public SqlSession session;
    private static final String thisProcess = "IndividualScore";

    @Override
    public SqlSession getSession() {
        return session;
    }

    public void setSession(SqlSession session) {
        this.session = session;
    }

    @Autowired
    private SqlSession getSessionMyBatis() throws IOException, SQLException {
        return new MyBatisUtil().getSession();
    }

    @Override
    public ProfileScoreResult getProfileScoreResult(String nome, int idade, String cpf, int dependentes, int renda) throws Exception {

        ProfileScoreResult result = new ProfileScoreResult();

        try {
            if (getSession() == null) {
                setSession(getSessionMyBatis());
            }
            ScoreRulesDAO scoreRulesDAO = getSession().getMapper(ScoreRulesDAO.class);
            result = scoreRulesDAO.getProfileScoreResult(nome, idade, cpf, dependentes, renda);
        } catch (IOException | SQLException | PersistenceException ex) {
            LogSLF4J.logException(this.getClass(), ex);
            throw new Exception((String) ex.getMessage().toString());
        }
        return result;
    }

    @Override
    public List<Object> getAllScoreRules() throws Exception {

        List<Object> result = null;

        try {
            if (getSession() == null) {
                setSession(getSessionMyBatis());
            }
            ScoreRulesDAO scoreRulesDAO = getSession().getMapper(ScoreRulesDAO.class);
            result = scoreRulesDAO.getAllScoreRules();
        } catch (IOException | SQLException | PersistenceException ex) {
            LogSLF4J.logException(this.getClass(), ex);
            throw new Exception((String) ex.getMessage().toString());
        }
        return result;
    }
}
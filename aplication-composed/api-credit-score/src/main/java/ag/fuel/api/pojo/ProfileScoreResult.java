package ag.fuel.api.pojo;

import java.io.Serializable;

public class ProfileScoreResult implements Serializable, Cloneable {

    private String Nome;
    private String CPF;
    private int Score;
    private int Resultado;

    public ProfileScoreResult() {

    }

    public ProfileScoreResult clone() {

        ProfileScoreResult clone = null;

        try {
            clone = (ProfileScoreResult) super.clone();
        } catch (CloneNotSupportedException e) {

            throw new RuntimeException(e);
        }

        return clone;
    }

    public ProfileScoreResult(String nome, String CPF, int score, int resultado) {
        Nome = nome;
        this.CPF = CPF;
        Score = score;
        Resultado = resultado;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    public int getResultado() {
        return Resultado;
    }

    public void setResultado(int resultado) {
        Resultado = resultado;
    }
}

SELECT rs.idade
     , rs.dependentes
     , rs.renda
     , score
FROM regras_score rs

SELECT rs.score, rr.resultado
FROM regras_score rs
         JOIN regras_resultado rr ON rs.score BETWEEN score_de AND score_ate
WHERE rs.idade = 20
  AND rs.dependentes = 0
  AND rs.renda = 5000
ORDER BY 1;

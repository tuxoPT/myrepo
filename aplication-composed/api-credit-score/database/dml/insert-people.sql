USE
[DB_A70863_creditanalyser];

TRUNCATE TABLE regras_score;

INSERT INTO regras_score
VALUES (20, 0, 5000, 350);
INSERT INTO regras_score
VALUES (18, 0, 1045, 600);
INSERT INTO regras_score
VALUES (25, 1, 3000, 450);
INSERT INTO regras_score
VALUES (53, 2, 10000, 880);
INSERT INTO regras_score
VALUES (65, 0, 2000, 800);
INSERT INTO regras_score
VALUES (32, 1, 6000, 300);

TRUNCATE TABLE regras_resultado;

INSERT INTO regras_resultado
VALUES (0, 400, 0);
INSERT INTO regras_resultado
VALUES (401, 600, 500);
INSERT INTO regras_resultado
VALUES (601, 650, 1000);
INSERT INTO regras_resultado
VALUES (651, 700, 1500);
INSERT INTO regras_resultado
VALUES (701, 750, 2000);
INSERT INTO regras_resultado
VALUES (751, 800, 2500);
INSERT INTO regras_resultado
VALUES (801, 850, 3000);
INSERT INTO regras_resultado
VALUES (851, 1000, 5000);
INSERT INTO regras_resultado
VALUES (1001, 10000, 10000);
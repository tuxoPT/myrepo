USE
[DB_A70863_creditanalyser];

DROP TABLE IF EXISTS regras_score;

CREATE TABLE regras_score
(
    idade       BIGINT NOT NULL,
    dependentes BIGINT NOT NULL,
    renda       BIGINT NOT NULL,
    score       BIGINT NOT NULL,
    PRIMARY KEY (idade, dependentes, renda)
);


DROP TABLE IF EXISTS regras_resultado;

CREATE TABLE regras_resultado
(
    score_de  BIGINT NOT NULL,
    score_ate BIGINT NOT NULL,
    resultado BIGINT NOT NULL,
    PRIMARY KEY (score_de, score_ate)
);

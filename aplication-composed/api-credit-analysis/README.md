### API de Análise de Credito

Microserviço Spring Boot que fará a comunicação com a API do Score


Acessar ao URL [**http://localhost:9002/**](http://localhost:9002/)

#### EndPoints Expostos ####

![picture](https://fuelag-my.sharepoint.com/personal/mauro_sousa_fuel_ag/Documents/projetos-java/upload-imagens/credit-analysis-api.png)


#### Exemplo de um resultado ####

![picture](https://fuelag-my.sharepoint.com/personal/mauro_sousa_fuel_ag/Documents/projetos-java/upload-imagens/credit-analysis-api-result.png)
/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.controllers;

import ag.fuel.api.dao.ApiServerStatusDAO;
import ag.fuel.api.dao.ProfileScoreResultDAO;
import ag.fuel.api.dao.impl.ApiServerStatusDAOImpl;
import ag.fuel.api.dao.impl.ProfileScoreResultDAOImpl;
import ag.fuel.api.pojo.Individual;
import ag.fuel.api.utils.GlobalProperties;
import ag.fuel.api.utils.LogSLF4J;
import ag.fuel.api.utils.RestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Mauro Sousa
 */
@Api(tags = "Servico Atribuicao Credito", description = "Obter Limite de Crédito")
@RestController
public class CreditAnalysisWebservicesController {

    @ApiOperation(value = "Obter o Resultado (Individual) do perfil de crédito", notes = "Serviço principal para cálculo de obtençaõ de crédito", response = RestResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK - Get values"),
            @ApiResponse(code = 204, message = "NO_CONTENT - Doesn't get values"),
            @ApiResponse(code = 500, message = "INTERNAL SERVER ERROR - An Exception Occurred")})
    @RequestMapping(path = "/get-perfil-credito-individual/{nome}/{idade}/{cpf}/{dependentes}/{renda}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<RestResponse> getIndividualProfileScore(@PathVariable(value = "nome") String nome, @PathVariable(value = "idade") int idade, @PathVariable(value = "cpf") String cpf, @PathVariable(value = "dependentes") int dependentes, @PathVariable(value = "renda") int renda) {

        List<Object> objectList = new ArrayList<>();

        RestResponse restResponse;
        RestResponse restServerResponse;
        ApiServerStatusDAO apiServerStatusDAO = new ApiServerStatusDAOImpl();

        try {

            //Check Server Status
            restServerResponse = apiServerStatusDAO.getServerStatus();
            if (restServerResponse.getHttpStatus().equals(HttpStatus.OK)) {

                //CALL to Score API EndPoint
                ProfileScoreResultDAO individualScoreDAO = new ProfileScoreResultDAOImpl();
                Individual individual = new Individual(nome, idade, cpf, dependentes, renda);

                restResponse = individualScoreDAO.getIndividualProfileScoreResult(individual.getNome(), individual.getIdade(), individual.getCPF(), individual.getDependentes(), individual.getRenda());

                return new ResponseEntity<>(restResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(restServerResponse, HttpStatus.OK);
            }

        } catch (Exception ex) {
            LogSLF4J.logException(this.getClass(), ex);
            restResponse = new RestResponse(LogSLF4J.getException(this.getClass(), ex), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), GlobalProperties.MESSAGE_ERROR);

            return new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Obter o Resultado (Lista) do perfil de crédito", notes = "Serviço principal para cálculo de obtençaõ de crédito", response = RestResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK - Get values"),
            @ApiResponse(code = 204, message = "NO_CONTENT - Doesn't get values"),
            @ApiResponse(code = 500, message = "INTERNAL SERVER ERROR - An Exception Occurred")})
    @RequestMapping(path = "/get-perfil-credito-lista", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<RestResponse> getListProfileScore() {

        List<Object> objectList = new ArrayList<>();

        RestResponse restResponse;
        RestResponse restServerResponse;
        ApiServerStatusDAO apiServerStatusDAO = new ApiServerStatusDAOImpl();

        try {

            //Check Server Status
            restServerResponse = apiServerStatusDAO.getServerStatus();
            if (restServerResponse.getHttpStatus().equals(HttpStatus.OK)) {
                //CALL to Score API EndPoint
                ProfileScoreResultDAO individualScoreDAO = new ProfileScoreResultDAOImpl();

                List<Individual> individuals = Arrays.asList(
                        new Individual("Pedro", 20, "18860265304", 0, 5000),
                        new Individual("João", 18, "68588672103", 0, 1045),
                        new Individual("Mauro", 40, "08263911176", 0, 20000),
                        new Individual("Teste1", 0, "000", 0, 2000),
                        new Individual(" ", 20, "000", 0, 2000),
                        new Individual("Test2", 0, "000", 0, 2000),
                        new Individual("Test3", 20, "000", 0, 0)
                );

                restResponse = individualScoreDAO.getListProfileScoreResult(individuals);

                return new ResponseEntity<>(restResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(restServerResponse, HttpStatus.OK);
            }

        } catch (Exception ex) {
            LogSLF4J.logException(this.getClass(), ex);
            restResponse = new RestResponse(LogSLF4J.getException(this.getClass(), ex), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), GlobalProperties.MESSAGE_ERROR);

            return new ResponseEntity<>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

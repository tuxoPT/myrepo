/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.dao.impl;

import ag.fuel.api.dao.ProfileScoreResultDAO;
import ag.fuel.api.pojo.Individual;
import ag.fuel.api.pojo.IndividualResult;
import ag.fuel.api.utils.GlobalProperties;
import ag.fuel.api.utils.LogSLF4J;
import ag.fuel.api.utils.RestResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mauro Sousa
 */
public class ProfileScoreResultDAOImpl implements ProfileScoreResultDAO {

    private static final String thisProcess = "ProfileScore";
    NumberFormat format = NumberFormat.getCurrencyInstance(GlobalProperties.BRAZIL);

    @Override
    public RestResponse getIndividualProfileScoreResult(String nome, int idade, String cpf, int dependentes, int renda) throws Exception {

        RestTemplate restTemplate = new RestTemplate();
        RestResponse restResponse;
        List<Object> objectList = new ArrayList<>();

        try {
            if (nome.isBlank() || cpf.isBlank() || idade == 0 || renda == 0) {
                Individual newIndividual = new Individual();
                newIndividual.setNome(nome);
                newIndividual.setCPF(cpf);
                objectList.add(new IndividualResult(nome, cpf, 0, format.format(0), "Dados inválidos para análise!"));
            } else {
                RestResponse myRestResponse = restTemplate.getForObject(GlobalProperties.API_HOST + ":" + GlobalProperties.API_PORT +
                                GlobalProperties.SLASH + GlobalProperties.API_METHOD +
                                GlobalProperties.SLASH + nome +
                                GlobalProperties.SLASH + idade +
                                GlobalProperties.SLASH + cpf +
                                GlobalProperties.SLASH + dependentes +
                                GlobalProperties.SLASH + renda
                        , RestResponse.class);

                String retNome = (String) ((LinkedHashMap) myRestResponse.getData()).get(GlobalProperties.NOME);
                String retCpf = (String) ((LinkedHashMap) myRestResponse.getData()).get(GlobalProperties.CPF);
                int retScore = (int) ((LinkedHashMap) myRestResponse.getData()).get(GlobalProperties.SCORE);
                int retResultado = (int) ((LinkedHashMap) myRestResponse.getData()).get(GlobalProperties.RESULTADO);

                String mensagem = new String();

                if (myRestResponse.getHttpStatus().equals(HttpStatus.OK) && myRestResponse.getMessage().equals(GlobalProperties.MESSAGE_SUCCESS)) {
                    if (retResultado == 0)
                        mensagem = "Sem direito a Crédito!";
                    else if (retResultado > 0)
                        mensagem = "Limite de Crédito Atribuído!";
                } else
                    mensagem = myRestResponse.getMessage();

                String formatedResult = format.format(retResultado);
                objectList.add(new IndividualResult(retNome, retCpf, retScore, formatedResult, mensagem));
            }
            restResponse = new RestResponse(objectList, HttpStatus.OK, HttpStatus.OK.value(), GlobalProperties.MESSAGE_SUCCESS);
        } catch (Exception ex) {
            LogSLF4J.logException(this.getClass(), ex);
            restResponse = new RestResponse(LogSLF4J.getException(this.getClass(), ex), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), GlobalProperties.MESSAGE_ERROR);
        }
        return restResponse;
    }

    @Override
    public RestResponse getListProfileScoreResult(List<Individual> individuals) throws Exception {

        RestTemplate restTemplate = new RestTemplate();
        RestResponse restResponse;
        List<Object> objectList = new ArrayList<>();


        try {
            objectList = individuals.stream().map(individual -> {
                        if (individual.getNome().isBlank() || individual.getCPF().isBlank() || individual.getIdade() == 0 || individual.getRenda() == 0) {
                            Individual newIndividual = new Individual();
                            newIndividual.setNome(individual.getNome());
                            newIndividual.setCPF(individual.getCPF());
                            return new IndividualResult(newIndividual.getNome(), newIndividual.getCPF(), 0, format.format(0), "Dados inválidos para análise!");
                        } else {
                            RestResponse myRestResponse = restTemplate.getForObject(
                                    GlobalProperties.API_HOST + ":" + GlobalProperties.API_PORT +
                                            GlobalProperties.SLASH + GlobalProperties.API_METHOD +
                                            GlobalProperties.SLASH + individual.getNome() +
                                            GlobalProperties.SLASH + individual.getIdade() +
                                            GlobalProperties.SLASH + individual.getCPF() +
                                            GlobalProperties.SLASH + individual.getDependentes() +
                                            GlobalProperties.SLASH + individual.getRenda()
                                    , RestResponse.class);
                            int retScore = (int) ((LinkedHashMap) myRestResponse.getData()).get(GlobalProperties.SCORE);
                            int retResultado = (int) ((LinkedHashMap) myRestResponse.getData()).get(GlobalProperties.RESULTADO);
                            String mensagem = new String();

                            if (myRestResponse.getHttpStatus().equals(HttpStatus.OK) && myRestResponse.getMessage().equals(GlobalProperties.MESSAGE_SUCCESS)) {
                                if (retResultado == 0)
                                    mensagem = "Sem direito a Crédito!";
                                else if (retResultado > 0)
                                    mensagem = "Limite de Crédito Atribuído!";
                            } else
                                mensagem = myRestResponse.getMessage();

                            String formatedResult = format.format(retResultado);
                            return new IndividualResult(individual.getNome(), individual.getCPF(), retScore, formatedResult, mensagem);
                        }
                    }
            ).collect(Collectors.toList());

            restResponse = new RestResponse(objectList, HttpStatus.OK, HttpStatus.OK.value(), GlobalProperties.MESSAGE_SUCCESS);

        } catch (Exception ex) {
            LogSLF4J.logException(this.getClass(), ex);
            restResponse = new RestResponse(LogSLF4J.getException(this.getClass(), ex), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), GlobalProperties.MESSAGE_ERROR);
        }
        return restResponse;
    }
}
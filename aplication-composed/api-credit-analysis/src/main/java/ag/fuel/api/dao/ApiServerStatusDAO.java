/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.dao;

import ag.fuel.api.utils.RestResponse;

/**
 * @author Mauro Sousa
 */
public interface ApiServerStatusDAO {

    RestResponse getServerStatus() throws Exception;

}

/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.utils;

import java.util.Locale;

/**
 * @author Mauro Sousa
 */

public class GlobalProperties {

    final public static String MESSAGE_SUCCESS = "Sucesso";
    final public static String MESSAGE_ERROR = "Erro";
    final public static String API_HOST = "//#{microservice-host}";
    final public static String API_PORT = "//#{microservice-port}";
    final public static String API_METHOD = "//#{microservice-method}";
    final public static String SERVER_STATUS_UP = "UP";
    final public static String SLASH = "/";
    final public static String NOME = "nome";
    final public static String CPF = "cpf";
    final public static String SCORE = "score";
    final public static String RESULTADO = "resultado";
    final public static Locale BRAZIL = new Locale("pt", "BR");
}

/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.dao.impl;

import ag.fuel.api.dao.ApiServerStatusDAO;
import ag.fuel.api.pojo.ApiStatus;
import ag.fuel.api.utils.GlobalProperties;
import ag.fuel.api.utils.LogSLF4J;
import ag.fuel.api.utils.RestResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @author Mauro Sousa
 */
public class ApiServerStatusDAOImpl implements ApiServerStatusDAO {

    private static final String thisProcess = "ApiServerStatus";

    @Override
    public RestResponse getServerStatus() throws Exception {

        RestTemplate restServerTemplate = new RestTemplate();
        RestResponse restResponse;

        try {
            //Check Server Status
            ResponseEntity<Object> serverResponse = restServerTemplate.getForEntity(GlobalProperties.API_HOST + ":" +
                    GlobalProperties.API_PORT + "/actuator/health/", Object.class);
            ApiStatus apiStatus = new ApiStatus();
            apiStatus.setServerStatus((String) ((LinkedHashMap) serverResponse.getBody()).get("status"));
            restResponse = new RestResponse(new ArrayList<>(), HttpStatus.OK, HttpStatus.OK.value(), GlobalProperties.MESSAGE_SUCCESS);
        } catch (Exception ex) {
            LogSLF4J.logException(this.getClass(), ex);
            restResponse = new RestResponse(new ArrayList<>(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.value(), "O EndPoint da API está indisponível ou ocupado. Tente mais tarde!");
        }
        return restResponse;
    }
}
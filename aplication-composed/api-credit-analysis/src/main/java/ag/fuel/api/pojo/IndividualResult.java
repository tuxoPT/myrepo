package ag.fuel.api.pojo;

import java.io.Serializable;

public class IndividualResult implements Serializable, Cloneable {

    private String Nome;
    private String CPF;
    private int Score;
    private String Resultado;
    private String Menssagem;

    public IndividualResult() {

    }

    public IndividualResult clone() {

        IndividualResult clone = null;

        try {
            clone = (IndividualResult) super.clone();
        } catch (CloneNotSupportedException e) {

            throw new RuntimeException(e);
        }

        return clone;
    }

    public IndividualResult(String nome, String CPF, int score, String resultado, String menssagem) {
        Nome = nome;
        this.CPF = CPF;
        Score = score;
        Resultado = resultado;
        Menssagem = menssagem;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int score) {
        Score = score;
    }

    public String getResultado() {
        return Resultado;
    }

    public void setResultado(String resultado) {
        Resultado = resultado;
    }

    public String getMenssagem() {
        return Menssagem;
    }

    public void setMenssagem(String menssagem) {
        Menssagem = menssagem;
    }
}

package ag.fuel.api.pojo;

import java.io.Serializable;

public class ApiStatus implements Serializable, Cloneable {

    private String serverStatus;

    public ApiStatus() {

    }

    public ApiStatus clone() {

        ApiStatus clone = null;

        try {
            clone = (ApiStatus) super.clone();
        } catch (CloneNotSupportedException e) {

            throw new RuntimeException(e);
        }

        return clone;
    }

    public ApiStatus(String serverStatus) {
        this.serverStatus = serverStatus;
    }

    public String getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(String serverStatus) {
        this.serverStatus = serverStatus;
    }
}

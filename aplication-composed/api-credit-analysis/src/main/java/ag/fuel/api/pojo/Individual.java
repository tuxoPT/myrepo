package ag.fuel.api.pojo;

import java.io.Serializable;

public class Individual implements Serializable, Cloneable {

    private String Nome;
    private String CPF;
    private int Idade;
    private int Dependentes;
    private int Renda;

    public Individual() {

    }

    public Individual clone() {

        Individual clone = null;

        try {
            clone = (Individual) super.clone();
        } catch (CloneNotSupportedException e) {

            throw new RuntimeException(e);
        }

        return clone;
    }

    public Individual(String nome, String CPF) {
        Nome = nome;
        this.CPF = CPF;
    }


    public Individual(String nome, int idade, String CPF, int dependentes, int renda) {
        Nome = nome;
        Idade = idade;
        this.CPF = CPF;
        Dependentes = dependentes;
        Renda = renda;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public int getIdade() {
        return Idade;
    }

    public void setIdade(int idade) {
        Idade = idade;
    }

    public int getDependentes() {
        return Dependentes;
    }

    public void setDependentes(int dependentes) {
        Dependentes = dependentes;
    }

    public int getRenda() {
        return Renda;
    }

    public void setRenda(int renda) {
        Renda = renda;
    }
}

/*
 * Copyright (c) 2021 Fuel.ag.
 * All rights reserved.
 * https://fuel.ag
 */
package ag.fuel.api.dao;

import ag.fuel.api.pojo.Individual;
import ag.fuel.api.utils.RestResponse;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author Mauro Sousa
 */
public interface ProfileScoreResultDAO {

    RestResponse getIndividualProfileScoreResult(@PathVariable(value = "nome") String nome, @PathVariable(value = "idade") int idade, @PathVariable(value = "cpf") String cpf, @PathVariable(value = "dependentes") int dependentes, @PathVariable(value = "renda") int renda) throws Exception;

    RestResponse getListProfileScoreResult(@PathVariable(value = "individuals") List<Individual> individuals) throws Exception;

}
